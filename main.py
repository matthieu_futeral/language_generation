# -*- coding: utf-8 -*-

"""
Created on Mon Oct 21 14:42:39 2019

@author: matthieufuteral-peter
"""

import os
import argparse
import torch
from transformers import *
from func import *

# models_name = 'CTRLModel'
# tokenizer_name = 'CTRLTokenizer'
# pretrained_weights = 'ctrl'

def main(input_sequence, sequence_len, k, model, tokenizer, T, device, model_type):

    sequence = []

    if model_type != 'xlnet':
        ids = tokenizer.encode(input_sequence)  # add_special_tokens=True)
    else:
        ids = tokenizer.encode(input_sequence + ' <mask>')
    # assert len(ids) >= window, "Sequence length must be longer than window size."
    X = torch.tensor([ids])
    if torch.cuda.is_available():
        X = X.to(device)

    with torch.no_grad():
        pred = pred_token(X, model=model, k=k, T=T, model_type=model_type)
        sequence.append(pred)

    if model_type != 'xlnet':
        sentence = ids + sequence
    else:
        sentence = ids[:-3] + sequence

    if sequence_len == 1:
        return tokenizer.decode(sentence)

    else:
        for _ in range(sequence_len - 1):

            if (_ + 1) % 10 == 0:
                print(_ + 1)

            X = torch.tensor([sentence])
            if torch.cuda.is_available():
                X = X.to(device)

            with torch.no_grad():
                pred = pred_token(X, model=model, k=k, T=T, model_type=model_type)
                sequence.append(pred)

            if model_type != 'xlnet':
                sentence = ids + sequence
            else:
                sentence = ids[:-3] + sequence

        return tokenizer.decode(sentence)


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument('--text', type=str)
    parser.add_argument('--k', type=int, default=5)
    parser.add_argument('--T', type=float, default=1)
    parser.add_argument('--sequence_len', type=int, default=10)
    parser.add_argument('--model', type=str, default='gpt2')
    args = parser.parse_args()

    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    print(device)
    print(torch.cuda.is_available())

    if args.model == 'gpt2':
        model = GPT2LMHeadModel.from_pretrained('gpt2')
        tokenizer = GPT2Tokenizer.from_pretrained('gpt2')
    elif args.model == 'xlnet':
        model = XLNetLMHeadModel.from_pretrained('xlnet-base-cased')
        tokenizer = XLNetTokenizer.from_pretrained('xlnet-base-cased')
    elif args.model == 'ctrl':
        model = CTRLLMHeadModel.from_pretrained('ctrl')
        tokenizer = CTRLTokenizer.from_pretrained('ctrl')  # First token reserved for topic (e.g. : Books, Links etc...)
    else:
        raise Exception('Model not found')

    if torch.cuda.is_available():
        model.to(device)

    model.eval()

    sentence = main(input_sequence=args.text, sequence_len=args.sequence_len, k=args.k, model=model,
                    tokenizer=tokenizer, T=args.T, device=device, model_type=args.model)

    print('\n', sentence, '\n')



# model = XLNetLMHeadModel.from_pretrained('xlnet-base-cased')
# model = XLMWithLMHeadModel.from_pretrained('xlm-mlm-enfr-1024')
# model = BertForMaskedLM.from_pretrained('bert-base-uncased')
# model = CTRLLMHeadModel.from_pretrained('./seqlen256_v1.bin')

