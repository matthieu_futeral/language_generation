# -*- coding: utf-8 -*-

"""
Created on Mon Oct 21 14:42:39 2019

@author: matthieufuteral-peter
"""

import os
import argparse
from logger import logger
import torch
from torch.utils.data import TensorDataset
import pandas as pd
import re
from transformers import BertForMaskedLM, BertTokenizer, RobertaForMaskedLM, RobertaTokenizer
from func import preprocessing

parser = argparse.ArgumentParser()
parser.add_argument('--model', type=str, default='bert-en')
args = parser.parse_args()

PATH_PROJECT = os.getcwd()
PATH_DATA = os.path.join(PATH_PROJECT, 'data')
if not os.path.exists(PATH_DATA):
    os.mkdir(PATH_DATA)

PATH_RESULT = os.path.join(PATH_PROJECT, 'result')
if not os.path.exists(PATH_RESULT):
    os.mkdir(PATH_RESULT)

df = pd.read_csv(PATH_DATA + '/en.txt', sep='\t', header=None)
df.columns = ['gender', 'n', 'sentence', 'antecedent']
df = df[df.gender != 'neutral']  # 1822 female & 1826 male  // df[(df.gender == 'male') | (df.gender == 'female')]
df = df.reset_index(drop=True)
text = list(df.sentence)
text = [txt.lower() for txt in text]

# Vocab count
vocab_count = {'he': 0, 'she': 0, 'him': 0, 'her': 0, 'his': 0}
for s in text:
    s = re.sub('\.', '', s)
    for w in vocab_count.keys():
        vocab_count[w] += s.split().count(w)

# Load model
if args.model == 'bert-en':
    tokenizer = BertTokenizer.from_pretrained('bert-base-cased')
    model = BertForMaskedLM.from_pretrained('bert-base-cased')
elif args.model == 'bert-multi':
    tokenizer = BertTokenizer.from_pretrained('bert-base-multilingual-cased')
    model = BertForMaskedLM.from_pretrained('bert-base-multilingual-cased')
elif args.model == 'roberta':
    tokenizer = RobertaTokenizer.from_pretrained('roberta-base')
    model = RobertaForMaskedLM.from_pretrained('roberta-base')
model.eval()

logger.info('Model and data loaded')

sample, sentences = preprocessing(text, tokenizer, model_type=args.model)
dataset = TensorDataset(sample)
loader = torch.utils.data.DataLoader(dataset,
                                     batch_size=16,
                                     num_workers=0)

logger.info('Preprocessing Done')
print('{} sentences'.format(len(text)))
print('{} sentences after preprocessing'.format(len(sentences)))

result = []
with torch.no_grad():
    for i, x_sample in enumerate(loader, 1):
        x_sample = x_sample[0]
        if i % 20 == 0:
            print("Batch n°{}".format(i))
        if args.model == 'roberta':
            prediction = model.forward(x_sample, attention_mask=(x_sample != 1) & (x_sample != 50264))[0]
            result += torch.argmax(prediction[x_sample == 50264], 1).tolist()
        else:
            prediction = model.forward(x_sample, attention_mask=(x_sample != 0) & (x_sample != 103))[0]
            result += torch.argmax(prediction[x_sample == 103], 1).tolist()

result = [tokenizer.decode(token).strip() for token in result]
tokens = list(set(result))
pred_tokens = {w: result.count(w) for w in tokens}
for tk in vocab_count.keys():
    if tk not in pred_tokens.keys():
        pred_tokens[tk] = 0

logger.info('Evaluation Done - start saving results ...')

df = pd.DataFrame({token: [vocab_count[token], pred_tokens[token]] for token in vocab_count.keys()},
                  index=['gold', 'mlm_pred']).T
df.index.name = 'word'
df.to_csv(PATH_RESULT + '/result_mlm_{}.txt'.format(args.model), sep='\t')

