# Language Generation

Nef path : /home/mfuteral/Inria/sosweet_1/language_generation

Files :
- **main.py** : generate text from autoregressif language models (causal), GPT2, CTRL.
- **mlm_generation.py** : generate gender mask tokens from the gender dataset (Masks are 'he', 'she', 'his', 'her', 'him'). Compute **#she/(#he + #she)** and **#her/(#her + #him + #his)** predictions for Bert English, Bert multilingual and RoBerta.

## Generate text for mask language model
```
python main.py --text He is a doctor, she is a --k 5 --T 0.7 --sequence_len 20 --model gpt2
``` 
### Parameters
- text : input sequence given to the model
- k : top k-predictions to generate outputs
- T : Temperature
- sequence_len : number of tokens to generate
- model : model name, either gpt2 or CTRL (for CTRL first token is a category)

## Generate Mask tokens for MLM
```
python mlm_generation.py --model bert-en
```
Compute ratios mentionned above for dataset [mt_gender - gabrielStanovsky](https://github.com/gabrielStanovsky/mt_gender/blob/master/data/aggregates/en.txt) and for Bert English, Bert multilingual and RoBerta.

### Parameters
- model : model name, **bert-en**, **bert-multi**, **roberta**
