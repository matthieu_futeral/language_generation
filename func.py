# -*- coding: utf-8 -*-

"""
Created on Mon Oct 21 14:42:39 2019

@author: matthieufuteral-peter
"""

import os
import re
import numpy as np
import torch
import torch.nn.functional as F
from tensorflow.python.keras.preprocessing import sequence


def top_k_pred(logits, k):
    if k < 1:
        return logits
    else:
        values, indices = torch.topk(logits, k)
        min_value = values[:, -1]
        return torch.where(logits < min_value, torch.ones_like(logits) * -1e20, logits)


def pred_token(X, model, k, T, model_type):

    if model_type != 'xlnet':
        logits = model(X)[0]
    else:
        perm_mask = torch.zeros((1, X.shape[1], X.shape[1]), dtype=torch.float)
        perm_mask[:, :, -1] = 1.0
        target_mapping = torch.zeros((1, 1, X.shape[1]), dtype=torch.float)
        target_mapping[0, 0, -1] = 1.0
        logits = model(X, perm_mask=perm_mask, target_mapping=target_mapping)

    logits = logits[:, -1, :]
    # pred = top_k_pred(logits, args.k)

    output, index = torch.topk(logits, k)
    index = index.flatten().tolist()  # output.squeeze(0).tolist()
    output = F.softmax(output/T, dim=1).squeeze(0).tolist()  # F.softmax(output.squeeze(0), dim=0)
    if sum(output) != 1.0:
        val = sum(output)
        output = [p/val for p in output]
    prediction = np.argmax(np.random.multinomial(1, output))

    return index[prediction]


def preprocessing(text, tokenizer, model_type):
    result = []
    sentences = []
    if model_type == 'roberta':
        mask_token = '<mask>'
    else:
        mask_token = '[MASK]'
    for i, s in enumerate(text):
        s = re.sub(r'\bhe\b', mask_token, s)
        s = re.sub(r'\bshe\b', mask_token, s)
        s = re.sub(r'\bhim\b', mask_token, s)
        s = re.sub(r'\bher\b', mask_token, s)
        s = re.sub(r'\bhis\b', mask_token, s)
        if mask_token not in s.split():
            continue
        else:
            sentences.append(s)
            result.append(tokenizer.encode(s))
    if model_type == 'roberta':
        result = sequence.pad_sequences(result, padding='post', dtype='int64', value=1)
    else:
        result = sequence.pad_sequences(result, padding='post', dtype='int64')
    return torch.tensor(result), sentences
