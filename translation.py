# -*- coding: utf-8 -*-

"""
Created on Mon Oct 21 14:42:39 2019

@author: matthieufuteral-peter
"""

import os
import pandas as pd
import time
from logger import logger
from google.cloud import translate_v2

os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = r'/Volumes/LaCie/Inria/sosweet_1/language_generation/google_cloud_key.json'
translator_client = translate_v2.Client()

PATH_PROJECT = os.getcwd()
PATH_DATA = os.path.join(PATH_PROJECT, 'data')
if not os.path.exists(PATH_DATA):
    os.mkdir(PATH_DATA)

df = pd.read_csv(PATH_DATA + '/en.txt', sep='\t', header=None)
df.columns = ['gender', 'n', 'sentence', 'antecedent']
df = df[df.gender != 'neutral']  # 1822 female & 1826 male  // df[(df.gender == 'male') | (df.gender == 'female')]
df = df.reset_index(drop=True)
text = list(df.sentence)
text = [txt.lower() for txt in text]


translation = []
f = open(PATH_DATA + '/fr.txt', 'w')
for i, sentence in enumerate(text, 1):
    if i % 10 == 0:
        print('Sentence n°{}'.format(i))
    time.sleep(0.5)
    output = translator_client.translate(sentence, target_language='fr')
    translation.append(output['translatedText'])
    f.write(output['translatedText'] + '\n')


